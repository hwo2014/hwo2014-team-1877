_ = require('lodash-node')
json2csv = require('json2csv')
fs = require('fs')
{Track} = require('./track')
{Metrics} = require('./metrics')
{RaceEvent} = require('./raceevent')

class Strategy

  constructor: (options) ->

    @power = 1
    @botName = ''
    @botColor = ''
    @carLength = 0
    @carGuideFlagPosition = 0
    @gameTick = 0
    @track = null
    @metrics = new Metrics
    @raceHistory = []

  update: (message) ->

    #@gameTick = message.gameTick ? 0
    if not message.gameTick?
      console.log '%j', message

    if @botName? and @botColor? and message.gameTick?

      @gameTick = message.gameTick

      myCar = _.find message.data, (car) ->
        car.id.name is @botName and car.id.color is @botColor
      , this

      myCar.dimensions = new Object()
      myCar.dimensions.length = @carLength
      myCar.dimensions.guideFlagPosition = @carGuideFlagPosition

      @currentPiece = @track.pieces[myCar.piecePosition.pieceIndex]

      @metrics.calculate myCar, @currentPiece

      #if Math.abs(myCar.angle) > @maxAngle
      #  @power = @resetPower
      #if Math.abs(myCar.angle) <= @maxAngle
      #  @power = @maxPower

      #if @currentPiece.type is 'straight' and @currentPiece.nextPiece().type is 'straight' and @currentPiece.nextPiece().nextPiece().type is 'straight'
      #  @power = 0.85
      #else if @currentPiece.type is 'straight' and @currentPiece.nextPiece().type is 'straight'
      #  @power = 0.83
      #else if @currentPiece.type is 'straight' and @currentPiece.nextPiece().type is 'angle'
      #  @power = 0.60

      length = 0
      if @currentPiece.type is 'straight'
        length = Metrics.getLengthToNextAnglePiece(@currentPiece.nextPiece(), myCar.piecePosition.inPieceDistance)
        if length > 275
          @power = 1
        else if length > 250
          @power = 0.8
        else if length > 225
          @power = 0.7
        else
          @power = 0.6
      else if Math.abs(myCar.angle) <= 5
        @power = 0.63
      else if Math.abs(myCar.angle) <= 10
        @power = 0.61
      else
        @power = 0.6

      #console.log @gameTick, myCar.piecePosition.pieceIndex, @power, @metrics.velocity, myCar.angle, length
      #console.log myCar.angle, @metrics.slipAngularVelocity
      raceEvent = new RaceEvent
        gameTick: @gameTick
        pieceIndex: myCar.piecePosition.pieceIndex
        power: @power
        velocity: @metrics.velocity
        angle: myCar.angle
        deltaSlipAngle: @metrics.deltaSlipAngle
        averageSlipAngle: @metrics.averageSlipAngle
        lengthToNextAngle: length
        slipArcLength: @metrics.slipArcLength
        lastSlipArcLength: @metrics.lastSlipArcLength
        slipAngularVelocity: @metrics.slipAngularVelocity

      @raceHistory.push raceEvent

  printHistory: ->

    json2csv
      data: @raceHistory
      fields: [
        'gameTick'
        'pieceIndex'
        'power'
        'velocity'
        'angle'
        'deltaSlipAngle'
        'averageSlipAngle'
        'lengthToNextAngle'
        'slipArcLength'
        'lastSlipArcLength'
        'slipAngularVelocity'
      ]
    , (err, csv) ->
      console.log err if err
      fs.writeFile "raceHistory-#{(new Date).toString()}.csv", csv, (err) ->
        throw err if err
        return

  serverMessage: ->

    return {msgType: 'throttle', data: @power}

exports.Strategy = Strategy
