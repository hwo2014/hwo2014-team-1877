_ = require('lodash-node')
{Metrics} = require('./metrics')

class Track

  constructor: (options) ->

    {@message} = options

    @pieces = @message.data.race.track.pieces
    [@firstPiece, ..., @lastPiece] = @pieces

    _.each @pieces, (piece, index, list) ->

      if piece.radius?
        piece.length = Metrics.getArcLength(piece.radius, Math.abs(piece.angle))
        piece.type = 'angle'
      else
        piece.type = 'straight'

      piece.index = index

      piece.previousPiece = () =>

        if index is @firstPiece.index
          return @lastPiece
        else
          return list[index-1]

      piece.nextPiece = () =>

        if index is @lastPiece.index
          return @firstPiece
        else
          return list[index+1]

    , this
    return

exports.Track = Track
