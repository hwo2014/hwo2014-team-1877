_ = require('lodash-node')
net = require('net')
JSONStream = require('JSONStream')
{Strategy} = require('./strategy')
{Track} = require('./track')

serverHost = process.argv[2]
serverPort = process.argv[3]
botName = process.argv[4]
botKey = process.argv[5]

console.log(botName, 'is connected to: ', serverHost + ':' + serverPort)

strategy = new Strategy

client = net.connect serverPort, serverHost, () ->
  send({ msgType: 'join', data: { name: botName, key: botKey }})

send = (json) ->
  client.write JSON.stringify(json)
  client.write '\n'

jsonStream = client.pipe(JSONStream.parse())

jsonStream.on 'data', (message) ->

  switch message.msgType
    when 'carPositions'
      strategy.update(message)
      send {msgType: 'throttle', data: strategy.power}
    when 'yourCar'
      strategy.botName = message.data.name
      strategy.botColor = message.data.color
      send {msgType: 'ping', data: {}}
    when 'gameInit'
      track = new Track message: message
      strategy.track = track
      myCar = _.find message.data.race.cars, (car) ->
        car.id.name = 'pikachu'
      , this
      strategy.carLength = myCar.dimensions.length
      strategy.carGuideFlagPosition = myCar.dimensions.guideFlagPosition
      send {msgType: 'ping', data: {}}
    when 'crash'
      console.log 'crash'
      send {msgType: 'ping', data: {}}
    when 'turboAvailable'
      console.log 'turboAvailable'
      send {msgType: 'ping', data: {}}
    when 'spawn'
      console.log 'spawn'
      send {msgType: 'ping', data: {}}
    when 'join'
      console.log 'join'
      send {msgType: 'ping', data: {}}
    when 'gameStart'
      console.log '%j', message
      send {msgType: 'ping', data: {}}
    when 'gameEnd'
      if process.env.HWO_DEV
        strategy.printHistory()
      console.log 'gameEnd %j', message.data.results
    else
      send {msgType: 'ping', data: {}}
      console.log '%j', message

jsonStream.on 'end', ->
  console.log 'end'

jsonStream.on 'error', ->
  console.log 'jsonStream.on error'
