class RaceEvent

  constructor: (options) ->

    {@gameTick, @pieceIndex, @power, @velocity, @angle, @deltaSlipAngle, @averageSlipAngle, @lengthToNextAngle, @slipArcLength, @lastSlipArcLength, @slipAngularVelocity} = options

exports.RaceEvent = RaceEvent
