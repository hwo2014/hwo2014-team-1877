_ = require('lodash-node')

class Metrics

  constructor: (options) ->

    @previousInPieceDistance = 0
    @previousPieceIndex = 0
    @slipAngleHistory = []
    @velocity = 0
    @deltaSlipAngle = 0
    @averageSlipAngle = 0
    @slipArcLength = 0
    @lastSlipArcLength = 0
    @slipAngularVelocity = 0

  calculate: (myCar, currentPiece) ->

    length = 0

    @slipAngleHistory.push myCar.angle

    subArray = _.last @slipAngleHistory, 2
    @averageSlipAngle = subArray.reduce((a,b) -> a + b) / subArray.length
    [first, ..., last] = subArray
    @deltaSlipAngle = Math.abs(last - first)

    #if currentPiece.type == 'angle'
    #  @slipArcLength = Metrics.getArcLength(myCar.dimensions.length - myCar.dimensions.guideFlagPosition, @deltaSlipAngle)
    #  @slipAngularVelocity = Math.abs(@slipArcLength - @lastSlipArcLength)
    #  @lastSlipArcLength = @slipArcLength
    #else
    @slipArcLength = 0
    @lastSlipArcLength = 0
    @slipAngularVelocity = 0


    if myCar.piecePosition.pieceIndex == @previousPieceIndex
      if myCar.piecePosition.inPieceDistance >= @previousInPieceDistance
        @velocity = myCar.piecePosition.inPieceDistance - @previousInPieceDistance
    else
      length = currentPiece.previousPiece().length
      @velocity = myCar.piecePosition.inPieceDistance + (length - @previousInPieceDistance)
      #console.log length, @previousInPieceDistance, myCar.piecePosition.inPieceDistance, @velocity

    @previousInPieceDistance = myCar.piecePosition.inPieceDistance
    @previousPieceIndex = myCar.piecePosition.pieceIndex
    return

  @getLengthToNextAnglePiece: (nextPiece, inPieceDistance) ->

    length = nextPiece.previousPiece().length - inPieceDistance

    while nextPiece.type == 'straight'
      length += nextPiece.length
      nextPiece = nextPiece.nextPiece()

    return length

  @getArcLength: (radius, angle) ->

    length = radius * angle * Math.PI / 180

exports.Metrics = Metrics
